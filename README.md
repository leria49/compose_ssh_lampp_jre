*******************************************************
#Présentation générale du projet compose-ssh-lampp-jre

*******************************************************

Voir la documentation complète: http://wiki.info.univ-angers.fr/aide:docker#presentation_generale_du_projet_compose-ssh-lampp-jre

Compose-ssh-lampp-jre est un projet permettant de provisionner facilement un ecosystème de type serveur web dans un container Docker. L'objectif de ce projet est double:

1. Permettre à des néophites en administration système de pouvoir disposer localement, facilement et sans effet de bord sur son installation originelle d'un environnement de développement web

2. Permettre à des administrateurs systèmes la mise en production d'applications web complètement isolé chacune les unes des autres, mais toutes déployées à partir de la même image (rationalisation du stockage et de l'environnement fourni) et avec la même procédure (grâce à docker-compose) et dans le même environnement (structure/organisation de fichier imposé).

Le projet crée et exploite une image docker permettant de provisionner un conteneur fournissant les services suivant:

* Apache
* MySql ou Postgresql
* openSSH
* Java Runtime Environnement (pour un usage éventuelle de Talend).

Cette liste peut-être amener à évouler en fonction des besoins à venir des utilisateurs.

**Fonctionnalitées, caractéristiques:**

* Il n'est pas obligatoire d'utiliser un serveur de base de donnée, mais on ne peut pas se servir de MySql et de PostgreSql en même temps, l'idée étant de ne pas pouvoir proposer plus d'une application par conteneur.

* Un utilisateur courant et les bases de données sont créé/importé au premier lancement du conteneur si nécéssaire: il y a une persistance des données de MySql, PostgreSql et de la home de l'utilisateurs dans des répertoires montés en tant que volume, si ces volumes sont déjà existant et contiennent déjà des données, les données sont réexploité.

* Un système de sauvegarde journalière permet de conserver un backup des bases de données pendant n jours. Cela permet de pouvoir récupérer les données en cas de système compromis. Il est toutefois conseillé de sauvegarder ces backups depuis un autre serveur afin de garantir une meilleure sécurité des donnée.

* Les processus éxécutés au démarrage du conteneur sont gérés par [supervisor](http://supervisord.org/): si un processus vient à mourir, supervisor tente de le remettre en route. 

**Spécificité:**

Afin de fournir à l'utilisateur final (celui qui se connectera en ssh dans son conteneur) un environnement proche de celui qu'il a l'habitude de cotoyer (serveur classique), nous avons fait le choix d'exécuter plusieurs processus sur le même conteneur, ce qui, pour certains, ne respecte pas "LA" philosophie docker qui consisterait, et cela fait toujours débat, à ne faire tourner qu'un processus par conteneur. Nous considérons qu'il n'y a pas "UNE" mais "DES" philosophies docker, et qu'il suffit d'adopter/créer l'approche la plus pertinente pour son besoin. La gestion de ces processus multiple est géré via [supervisor](http://supervisord.org/) comme conseillé par [la documentation officiel de docker](https://docs.docker.com/engine/admin/using_supervisord/).


*******************************************************
##Prérequis

*******************************************************
Installations de:

* [docker-engine](https://docs.docker.com/engine/installation/linux/ubuntulinux/)

Pour ne pas avoir a utiliser sudo à chaque commande docker depuis son login:

    usermod -aG docker jmc

On peut vérifier que tout fonctionne correctement: 

    docker run hello-world

* [docker-compose](https://docs.docker.com/compose/install/)

 
```
#!bash

 # En root
  curl -L https://github.com/docker/compose/releases/download/1.7.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
  chmod +x /usr/local/bin/docker-compose

```

* compose_ssh_lampp_jre

```
#!bash

git clone https://leria49@bitbucket.org/leria49/compose_ssh_lampp_jre.git name_of_your_app
cd name_of_your_app
chmod +x init.sh
./init.sh
```

Avant de construire votre image docker, il vous faut :

* importer vos sources dans le répertoire ./src (obligatoire)
* modifier le fichier ./.env afin que celui-ci corresponde à vos besoins (obligatoire)
* modifier le fichier ./etc/apache2/sites-available/apache_default afin que celui-ci corresponde à votre application web (obligatoire)
* placer la base de donnée au format .sql à importer dans le repertoire ./database2import (facultatif)
* importer votre documentation dans le répertoire ./documentation (facultatif)

Compilation:

    docker-compose build


*******************************************************
## Lancer votre application web dans un conteneur

*******************************************************

    docker-compose up -d 
    # -d permer de détacher l'exécution du conteneur du shell

Les sources présentent:

*dans le répertoire ./src sur l'hote
*dans le repertoire /home/username/public_html ou /var/www/html dans le conteneur

sont modifiable et les modifications sont prises en compte en temps réel(i.e.: sans avoir à relancer le conteneur).

C'est fini ! Vous disposez d'un serveur web exécutant le code présent dans ./src et disposant d'une sauvegarde automatique journalière (à 22h22) si nécéssaire (i.e.: si modification du contenu entre deux sauvegardes) de vos fichiers sources, de votre base de donnée et de votre home. Ces sauvegardes sont  disponible dans le répertoire backup_name_of_your_app de votre arborescence.

Voir la documentation complète: http://wiki.info.univ-angers.fr/aide:docker#presentation_generale_du_projet_compose-ssh-lampp-jre