#!/bin/bash

set -e

mkdir src database2import documentation

echo ""
echo "Avant de construire votre image docker, il vous faut :"
echo -e "\t importer vos sources dans le répertoire ./src (obligatoire)"
echo -e "\t placer la base de donnée au format .sql à importer dans le repertoire ./database2import (facultatif)"
echo -e "\t importer votre documentation dans le répertoire ./documentation (facultatif)"
echo -e "\t modifier le fichier ./.env afin que celui-ci corresponde à vos besoins (obligatoire)"
echo -e "\t modifier le fichier  ./etc/apache2/sites-available/apache_default afin que celui-ci corresponde à votre application web (obligatoire)"
echo ""

exit 0

