#!/bin/bash

set -e

. $(dirname ${0})/global_function.sh

echo "===> Tentative d'importation de la base de donnée ${MYSQL_DATABASE_NAME}.sql si celle-ci n'est pas déjà importé"
if [ -e ${PATH_MYSQL_DATABASE}/${MYSQL_DATABASE_NAME} ]; then
    echo "===> La base de donnée ${MYSQL_DATABASE_NAME} est déjà présente"
    echo "===> Rien à faire"
    exit 0
else
    echo "===> La base de donnée ${MYSQL_DATABASE_NAME} n'est pas déjà importé"
    echo "===> Création de la base ..."
    mysql -u root --execute="CREATE DATABASE ${MYSQL_DATABASE_NAME}"
    readonly R1=$?

    if isEmptyDir ${PATH_CONTAINER_IMPORT} ; then
        echo "===> Il n'y a pas de base de donnée disponible à l'importation"
        if [ $R1 ]; then
            echo "===> La base de donnée ${MYSQL_DATABASE_NAME} a été crée et est vide."
        fi
    elif [ -e ${PATH_CONTAINER_IMPORT}/${MYSQL_DATABASE_NAME}.sql ]; then
        echo "===> Tentative d'importation de ${MYSQL_DATABASE_NAME}.sql ..."
        mysql -u root ${MYSQL_DATABASE_NAME} < ${PATH_CONTAINER_IMPORT}/${MYSQL_DATABASE_NAME}.sql
        if [ $R1 ] && [ $? ]; then 
            echo "===> La base de donnée ${MYSQL_DATABASE_NAME}.sql a  bien été importé"
        fi
    else
        echo "===> Le fichier ${PATH_CONTAINER_IMPORT}/${MYSQL_DATABASE_NAME}.sql n'existe pas."
    fi
fi

