#!/bin/bash

# FUNCTION

# DEBUG
set -e

# Importation des fonctions générique
. $(dirname ${0})/global_function.sh

# VARIABLE
readonly PATH_TO_SAVE_AUTHENTIFICATION=/root/pgsql_authentification.txt

# PROGRAM

su postgres -c "usr/lib/postgresql/9.4/bin/postgres -D /var/lib/postgresql/9.4/main -c config_file=/etc/postgresql/9.4/main/postgresql.conf" 2>/dev/null & 
sleep 5

if su postgres -c "psql postgres -tAc \"SELECT 1 FROM pg_roles WHERE rolname='${PGSQL_USER_NAME}'\" | grep -q 1"; then
    echo "L'utilisateur ${PGSQL_USER_NAME} existe déjà"
    echo "Vous ne devriez pas être dans ce cas !"
else
    echo "==> Création de l'utilisateur local ${PGSQL_USER_NAME} ..."
    su postgres -c "psql -c \"CREATE USER ${PGSQL_USER_NAME} WITH PASSWORD '${PGSQL_PASSWORD}';\""
    save_user_authentification ${PGSQL_USER_NAME} ${PGSQL_PASSWORD} ${PATH_TO_SAVE_AUTHENTIFICATION}
fi

echo "==> Analyse des éventuelles bases de données existante"
if su postgres -c "psql -lqt" | cut -d \| -f 1 | grep -qw ${PGSQL_DATABASE_NAME}; then
    echo "==> La base de donnée ${PGSQL_DATABASE_NAME} existe déjà dans le répertoire monté comme volume: ${VOLUME_PGSQL}"
    "
    echo "==> Pas d'importation"
else
    echo "==> La base de donnée ${PGSQL_DATABASE_NAME} n'a pas déjà été importé"
    if isEmptyDir ${PATH_CONTAINER_IMPORT}; then
        echo "Il n'y a pas de base de donnée ${PGSQL_DATABASE_NAME}.sql dans ${PATH_CONTAINER_IMPORT}"
        echo "==> Pas d'importation"
        echo "Arret de postgres ..."
        killall postgres
        sleep 5
        exit 1
    else
        echo "==> Tentative d'importation de la base de donnée ${PGSQL_DATABASE_NAME}.sql ..."
        su postgres -c "psql --username=${PGSQL_USER_NAME} ${PGSQL_DATABASE_NAME} -f ${PATH_CONTAINER_IMPORT}/${PGSQL_DATABASE_NAME}.sql"
    fi
fi

echo "Arret de postgres ..."
killall postgres
sleep 5

echo ""
echo "==============================================================================="
# TODO: EXPOSE_PGSQL_DATABASE
if ${EXPOSE_PGSQL_DATABASE}; then 
    echo ""
    echo "La fonctionnalité d'exposition d'une base de donnée PGSQL n'a pas encore"
    echo "été implémenté dans notre image docker."
fi

echo ""
echo "L'utilisateur PostgreSql ${PGSQL_USER_NAME}" a été créé.
echo ""

echo ""
echo "Vous pouvez trouvez le  détails des authenfications des utilisateurs PgSql"
echo "dans le fichier ${PATH_TO_SAVE_AUTHENTIFICATION}"
echo ""
echo "==============================================================================="

service postgresql stop
sleep 5
