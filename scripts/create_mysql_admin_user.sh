#!/bin/bash

# FUNCTION


create_mysql_distant_user() {
    # NB_CHAR = $1 si définit, DEFAULT sinon
    local readonly DEFAULT=12
    local readonly NB_CHAR=${1:-$DEFAULT}

    local readonly PASSWORD=$(makepasswd --char ${NB_CHAR})
    
    echo "==> Création de l'utilisateur mysql 'admin' avec un mot de passe aléatoire"
    mysql -u root --execute="CREATE USER 'admin'@'%' IDENTIFIED BY '$PASSWORD'"
    mysql -u root --execute="GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' WITH GRANT OPTION"

    # PATH_TO_SAVE_AUTHENTIFICATION peut-être une variable globale
    if [ ! -z $PATH_TO_SAVE_AUTHENTIFICATION ]; then
        # On vérifie l'existence de la fonction
        if [ $(type -t save_user_authentification) == "function" ]; then
            save_user_authentification "admin" ${PASSWORD} ${PATH_TO_SAVE_AUTHENTIFICATION}
        else
            echo "--> Vous devez importer la fonction save_user_authentification()"
            exit 1
        fi
    fi
}
# DEBUG
set -e

# Import des fonctions génériques
. $(dirname ${0})/global_function.sh

# VARIABLE
readonly PATH_TO_SAVE_AUTHENTIFICATION=/root/mysql_authentification.txt

# flag
MYSQL_USER_CREATE=false

# PROGRAM

start_mysqld_safe

if ${EXPOSE_MYSQL_DATABASE}; then
    create_mysql_distant_user 10
fi


# Initialisation de la base de donnée et de l'utilisateur mysql associé si précisé
if [ -f /root/scripts/mysql-setup.sh ] && [ -x /root/scripts/mysql-setup.sh ]; then
    /root/scripts/mysql-setup.sh
    # On crée un utilisateur local autre que root si celui-ci est désiré, on lui alloue tout les droits sur la base de donnée importé 
    # Cela peut être pratique lorsque l'on ne veut pas changer la configuration d'une application existante
    if [ ! -z ${MYSQL_USER_NAME} ] && [ ! -z ${MYSQL_PASSWORD} ]; then
        echo "==> Création de l'utilisateur ${MYSQL_USER_NAME}"
        mysql -u root --execute="CREATE USER '${MYSQL_USER_NAME}'@'localhost' IDENTIFIED BY '${MYSQL_PASSWORD}'"   
        mysql -u root --execute="GRANT ALL PRIVILEGES ON ${MYSQL_DATABASE_NAME}.* TO '${MYSQL_USER_NAME}'@'localhost' WITH GRANT OPTION"
        save_user_authentification ${MYSQL_USER_NAME} ${MYSQL_PASSWORD} ${PATH_TO_SAVE_AUTHENTIFICATION}
        MYSQL_USER_CREATE=true
    elif [ ! -z ${MYSQL_USER_NAME} ]; then
        echo "--> Echec de la création de l'utilisateur ${MYSQL_USER_NAME}"
        echo "--> Vous devez spécifier un mot de passe pour cet utilisateur dans une variable d'environnement \
            MYSQL_PASSWORD dans votre Dockerfile et/ou docker-compose.yaml "
        exit 1
    fi
else
    echo "==> Il n'y a pas de script d'importation de base de donnée ou bien celui ci n'est pas exécutable"
fi

echo ""
echo "==============================================================================="
if ${EXPOSE_MYSQL_DATABASE}; then 
    echo ""
    echo "    mysql -u admin -p (voir ${PATH_TO_SAVE_AUTHENTIFICATION}) -h<host> -P<port>"
    echo ""
fi
if ${MYSQL_USER_CREATE}; then
    echo "L'utilisateur MySql ${MYSQL_USER_NAME}" a été créé.
    echo ""
fi 
echo "L'utilisateur MySQL 'root' n'a pas de mot de passe mais seul les "
echo "connexions locales sont acceptés."
echo ""
echo "Vous pouvez trouvez le  détails des authenfications des utilisateurs MySql"
echo "dans le fichier ${PATH_TO_SAVE_AUTHENTIFICATION}"
echo ""
echo "Si vous souhaitez un accès distant sur les bases de donnée, "
echo "vous devriez instancier la variable d'environnement EXPOSE_DATABASE à true"
echo "dans votre Dockerfile et/ou dans votre docker-compose.yaml et relancer la "
echo "procédure de construction ( docker-compose up --build )"
echo "==============================================================================="


mysqladmin -uroot shutdown
