#!/bin/bash

# Nom utilisateur bdd en minuscule
PGSQL_USER_NAME=$(echo ${PGSQL_USER_NAME} | tr A-Z a-z 2>/dev/null)
MYSQL_USER_NAME=$(echo ${MYSQL_USER_NAME} | tr A-Z a-z 2>/dev/null)

# Sauvegarde des variables d'environnements dans /root/.env pour une récupération possible par cron
# Doit être en dur pour ne pas avoir à modifier le script backup
env > /root/.env

# Droits d'execution sur les scripts présents dans le répertoire monté /root/scripts
chmod u+x /root/scripts/*

if ${IS_MYSQL_APPLICATION} && ${IS_PGSQL_APPLICATION}; then
    echo "Soit l'application se sert de mysql, soit elle se sert de postgresql"
    echo "Nous n'acceptons pas l'utilisation des 2 serveurs dans le même conteneur"
    echo "car nous souhaitons une application/un service par conteneur."
    exit 1
fi

# Configuration php
sed -ri -e "s/^upload_max_filesize.*/upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}/" \
    -e "s/^post_max_size.*/post_max_size = ${PHP_POST_MAX_SIZE}/" /etc/php5/apache2/php.ini

readonly FLAG=/root/.doNotEraseFlag

# Au premier lancement du conteneur
if [ ! -e "${FLAG}" ]; then
    echo "=> Premier lancement du conteneur"
    echo "=> Création de l'utilisateur ${USER_NAME} ..."
    if [ -d /home/${USER_NAME} ]; then
        useradd --shell /bin/bash ${USER_NAME}
        echo "=> Le répertoire /home/${USER_NAME} est déjà présent dans le répertoire monté comme volume: ${VOLUME_HOME}"
    else
        useradd --create-home --shell /bin/bash ${USER_NAME}
    fi
    echo ${USER_NAME}:${USER_PASSWORD} | chpasswd
    usermod -aG www-data ${USER_NAME}
    usermod -aG mysql ${USER_NAME}
    usermod -aG postgres ${USER_NAME}
    echo "User name:\t"${USER_NAME}"\nPassword:\t"${USER_PASSWORD}"" > /root/user_authentification.txt
    echo "=> Le mot de passe de ${USER_NAME} est disponible dans /root/user_authentification.txt"
    # Lien symbolique permettant de se lier aux sources de l'application depuis le home de l'utilisateur
    if [ ! -e /home/${USER_NAME}/public_html ]; then
        ln -s ${PATH_CONTAINER_SRC} /home/${USER_NAME}/public_html
    fi
    # Droits sur les sources de l'application web
    chown -R ${USER_NAME}:www-data ${PATH_CONTAINER_SRC}

    echo "=> Installation des bases de données si nécéssaire ..."
    # Soit installation base de donnée mysql
    if ${IS_MYSQL_APPLICATION}; then
        if [[ ! -d "${PATH_MYSQL_DATABASE}"/mysql ]]; then
            echo "=> Le répertoire "${PATH_MYSQL_DATABASE}" utilisé par MySql n'a pas été initialisé"
            echo "=> Initialisation de MySQL ..."
            mysql_install_db > /dev/null 2>&1
            echo "=> MySql a bien été initialisé"  
            /root/scripts/create_mysql_admin_user.sh
        else
            echo "=> Le  répertoire "${PATH_MYSQL_DATABASE}" est déjà présent dans le répertoire monté comme volume: ${VOLUME_MYSQL}"
        fi
    elif ${IS_PGSQL_APPLICATION}; then
        # Ou bien installation base de donnée pgsql
        /root/scripts/create_pgsql_user_and_setup_database.sh
    else
        echo "=> Aucune base de donné ne vas être utilisé."
        echo "=> Si vous souhaitez utilisez une base de donnée de mysql ou postgresql, "
        echo "=> il vous faut modifier les variables d'environnement dans le fichier ./.env"
    fi

# On pose le drapeau pour ne pas refaire la manipulation lors du prochain lancement du conteneur
touch "${FLAG}"
fi
echo ""
if [ ${IPV4_ADDRESS} ]; then
    echo "L'adresse ip de votre container est : ${IPV4_ADDRESS}"
fi
echo "=> Vous pouvez accéder à vos pages web sur http://${IPV4_ADDRESS}:${PORT_APACHE}"
echo "=> L'accès ssh se fait via l'utilisateur ${USER_NAME} sur le port ${PORT_SSH}"
if [ ! -z ${PORT_MYSQL} ]; then 
    echo "=> Vous pouvez vous connecter à distance sur le serveur MySql via le port ${PORT_MYSQL}"
fi

echo ""
echo "=> Lancement de supervisor ..."
echo ""



if ${IS_MYSQL_APPLICATION}; then
    exec supervisord -n -c /etc/supervisor/conf.d/supervisord_mysql.conf
elif "${IS_PGSQL_APPLICATION}"; then
    exec supervisord -n -c /etc/supervisor/conf.d/supervisord_pgsql.conf
else
    echo "=> Aucune base de donnée n'est active dans ce container !"
    exec supervisord -n -c /etc/supervisor/conf.d/supervisord_noDataBase.conf
    echo "=> Apache, openSSH et cron seulement."
    echo "=> Peut-être avez vous oublié de modifier des variables d'environnements dans ./.env ..."
fi

