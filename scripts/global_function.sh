#!/bin/bash

save_user_authentification() {
    local readonly USER="${1}"
    local readonly PASSWORD="${2}"
    local readonly FILE="${3}"

    if [ -z ${USER} ]; then
        echo "--> Il manque le nom d'utilisateur"
        exit 1
    fi
    
    if [ -z ${PASSWORD} ]; then
        echo "--> Il manque le mot de passe de ${USER}"
        exit 1
    fi

    if [ -z ${FILE} ]; then
        echo "--> Il manque le chemin du fichier dans lequel vous souhaitez sauvegarder les données"
        exit 1
    fi
    
    echo "==> Sauvegarde du mot de passe de admin dans "${FILE}""
    echo -e "User:\t ${USER}" >> ${FILE}
    echo -e "Password:\t ${PASSWORD}" >> ${FILE}
    echo "==> L'authentification de ${USER} est disponible dans ${FILE}"

}

start_mysqld_safe() {
/usr/bin/mysqld_safe > /dev/null 2>&1 &

local RET=1

while [[ RET -ne 0 ]]; do
    echo "==> Attente de confirmation du démarrage de mysql ..."
    sleep 5
    mysql -u root --execute "status" > /dev/null 2>&1
    RET=$?
done
}

isEmptyDir()
{
    if [ ! $# -eq 1 ]; then
        echo "---> Vous devez passer le nom d'un répertoire en argument"
        exit 1
    fi

    local dir_name="${1}"

    if [ "$(ls -A ${dir_name})" ]; then
        return 1;
    else
        return 0;
    fi
}

