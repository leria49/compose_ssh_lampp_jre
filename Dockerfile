FROM leria/ssh_lampp_jre
MAINTAINER Jean-Mathieu Chantrein jean-mathieu.chantrein@univ-angers.fr

#################################### Instruction de compilation ##############################################
#
#   1) Placez vos fichiers sources web dans ./src
#   2) Placez votre éventuelle base de donnée au format .sql dans ./data2import
#   3) Placez votre documentation (Rapports, instructions, ...) dans ./documentation
#   4) Modifiez les variables dans le fichier ./.env à votre convenance
#   5) Eventuellement, faites de même dans le fichier ./docker-compose.yaml
#   6) Lancez la commande suivante:
#
#       docker-compose up
#
##############################################################################################################

# Configuration mysql, supervisor, apache2 et installation scripts de post-build
COPY etc/crontab /etc/crontab
COPY etc/mysql/my.cnf /etc/mysql/conf.d/my.cnf
COPY etc/supervisor/* /etc/supervisor/conf.d/
COPY etc/apache2/sites-available/apache_default /etc/apache2/sites-available/000-default.conf
COPY scripts/run.sh /run.sh

# Argument instancier dans le fichier ./.env
ARG APACHE_SERVER_NAME

RUN echo "ServerName ${APACHE_SERVER_NAME}" >> /etc/apache2/apache2.conf && \
    a2enmod rewrite && \
    chmod u+x /run.sh
